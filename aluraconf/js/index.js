function SmoothScroll ( selector ) {

    $ ( selector ).click ( function ( event ) {

        event.preventDefault ( );
        var target = $ ( this ).attr ( 'href' );

        $ ( 'html, body' ).animate ( {
            scrollTop: $ ( target ).offset ( ).top
        }, 1000 )

    } );
}

SmoothScroll ( 'a[href*=panel-about]' );
SmoothScroll ( 'a[href*=panel-speakers]' );
SmoothScroll ( 'a[href*=panel-form]' );